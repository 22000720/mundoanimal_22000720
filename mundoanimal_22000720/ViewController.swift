//
//  ViewController.swift
//  mundoanimal_22000720
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Animal :  Decodable{
    let name: String
    let latin_name: String
    let image_link: String
}

class ViewController: UIViewController {
    @IBOutlet var ImageOutlet: UIImageView!
    @IBOutlet var LatinNameOutlet: UILabel!
    var data : Animal?
    @IBAction func ReloadDataButton(_ sender: Any) {
        requestData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        requestData()
    }

    private func requestData(){
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
            response in
            if let data_request = response.value{
                self.data = data_request;
            }
            self.LatinNameOutlet.text = self.data?.latin_name
            self.ImageOutlet.kf.setImage(with: URL(string: self.data!.image_link))
            self.title = self.data?.name
        }
    }
    
}

